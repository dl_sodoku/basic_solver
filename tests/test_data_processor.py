# Copyright George Barnett 2020

"""data processing

Basic class for training and evaluating model
"""

# Basic Imports
from pathlib import Path
import csv
import random

# External Imports
from typing import List, Any

import numpy as np
import pytest

# Internal Imports
from sodokuAI.data import raw_data_processor as data_processor

# Global Variables
# n/a


@pytest.fixture
def foo_bar_csv(tmp_path: Path):
    headers = ("name", "value")
    data = [("Foo", 0), ("Bar", 1)]
    csv_path = tmp_path / "test.csv"
    with open(csv_path, "w") as f:
        csv_writer = csv.writer(f)
        csv_writer.writerow(headers)
        csv_writer.writerows(data)
    yield csv_path
    csv_path.unlink(missing_ok=True)


def test_convert_int():
    converted = data_processor.convert_str_to_arr("1234", board_size=(2, 2))
    assert np.all(converted == np.array([[1, 2], [3, 4]]))


def test_characters_in_string():
    with pytest.raises(ValueError):
        data_processor.convert_str_to_arr("1a23", board_size=(2, 2))


def test_incorrect_size():
    with pytest.raises(ValueError, match="cannot reshape array "):
        data_processor.convert_str_to_arr("123", board_size=(2, 2))


def test_a_b_sample():
    data = list(range(20))
    # check for multiple random seeds
    for seed in [23, 5, 8, 9]:
        random.seed(seed)
        a_output, b_output = data_processor.a_b_random_sample(data, 0.5)
        overlap = set(a_output).intersection(set(b_output))
        assert len(overlap) == 0


def test_split_data_splits(monkeypatch):
    data = list(range(10))

    def mock_sample(data: List[Any], proportion: float):
        proportion_int = int(len(data) * proportion)
        return data[:proportion_int], data[proportion_int:]

    monkeypatch.setattr(data_processor, "a_b_random_sample", mock_sample)

    output_splits = data_processor.split_data(data, [0.6, 0.1, 0.3])
    assert output_splits == [[0, 1, 2, 3, 4, 5], [6,], [7, 8, 9]]


def test_split_data_invalid_proportinos():
    with pytest.raises(ValueError):
        data_processor.split_data([1, 2, 3], [0.3, 0.8])
