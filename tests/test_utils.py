"""Testing Utils

tests for validating sudoku if they are completed or not
"""

# Basic Imports
import pytest
import copy

# External Imports
import torch
import numpy as np

# Internal Imports
from sodokuAI import utils

# Global Variables
# n/a


def rolling_solved_array(board_size: int) -> np.ndarray:
    puzzle = []
    num_subgrids = int(board_size ** 0.5)
    for i in range(0, num_subgrids):
        for j in range(0, num_subgrids):
            rolled_row = np.roll(np.arange(1, board_size + 1), i + j * num_subgrids)
            puzzle.append(rolled_row)
    return np.array(puzzle)


@pytest.fixture
def solved_9x9_arr() -> np.ndarray:
    """solved 9,9 rolling puzzle
    
    Top row is (1..9), bottom row (9,1...8)
    """
    yield rolling_solved_array(9)


@pytest.fixture
def solved_9x9_arr_t(solved_9x9_arr) -> torch.Tensor:
    """Tensor version of solved_9x9_arr"""
    yield copy.copy(torch.tensor(solved_9x9_arr))


class TestElementwiseAccuracy:
    def test_correct_puzzle(self, solved_9x9_arr_t: torch.Tensor) -> None:
        p = solved_9x9_arr_t.clone()
        t = solved_9x9_arr_t.clone()

        output = utils.ElementwiseAccuracy(reduction="mean")(prediction=p, target=t)
        assert output == 1

    def test_swapped_elements(self, solved_9x9_arr_t: torch.Tensor) -> None:
        p = solved_9x9_arr_t.clone()
        t = solved_9x9_arr_t.clone()

        # swap first two elements in prediction
        p[0, 0:2] = torch.flip(p[0, 0:2], [0])

        output = utils.ElementwiseAccuracy(reduction="mean")(prediction=p, target=t)
        assert output == (t.numel() - 2) / t.numel()
