# Copyright George Barnett 2020

"""puzzle class testing

Run tests for puzzle class object testing
"""

# Basic Imports
import pytest

# External Imports
import numpy as np

# Internal Imports
from sodokuAI.puzzle import Puzzle

# Global Variables
# n/a


@pytest.fixture
def arr_4x4() -> np.ndarray:
    """ example 4x4 array

    An numpy array containing a 4x4 array of the numbers 1->16

    Returns:

    """
    yield np.arange(1, 17).reshape([4, 4])


@pytest.fixture
def puzzle_4x4(arr_4x4) -> Puzzle:
    """ example 4x4 puzzle

    Use the Puzzle.from_array(arr) to create a puzzle object containing . Check that the test_puzzle_from_array is passing to ensure
    this fixtures works properly

    Returns:
        Puzzle class with a board that is solved and valid
    """
    puzzle = Puzzle.from_numpy(arr_4x4)
    yield puzzle


# TEST Basic Initialisation
def test_puzzle_creation():
    assert np.all(
        Puzzle(9).array == np.zeros([9, 9])
    ), "Initialisation of 9x9 zeros array in puzzle"
    assert np.all(
        Puzzle(4).array == np.zeros([4, 4])
    ), "Initialisation of 9x9 zeros array in puzzle"


def test_nonsquare_board_size():
    with pytest.raises(ValueError):
        Puzzle(5)


def test_board_shape():
    assert Puzzle(9).shape == (9, 9), "Error on getting board shape"
    assert Puzzle(4).shape == (4, 4), "Error on getting board shape"


def test_board_sub_grid_shape():
    assert Puzzle(9).sub_grid_shape == (3, 3), "Failed on incorrect sub-grid shape"


# Test Creating from numpy array
def test_numpy_creation(arr_4x4):
    puzzle = Puzzle.from_numpy(arr_4x4)
    assert np.all(puzzle.array == arr_4x4)
    assert puzzle.shape == (4, 4)
    assert puzzle.sub_grid_shape == (2, 2)


def test_invalid_type_numpy(arr_4x4):
    for np_type in [np.float32, np.float16, np.uint8, np.complex]:
        with pytest.raises(TypeError):
            Puzzle.from_numpy(np_type(arr_4x4))


# TEST Functionality
def test_get_row(puzzle_4x4, arr_4x4):
    assert np.all(puzzle_4x4.row(1) == arr_4x4[1])


def test_neg_row(puzzle_4x4, arr_4x4):
    assert np.all(puzzle_4x4.row(-1) == arr_4x4[-1])


def test_invalid_row(puzzle_4x4):
    with pytest.raises(IndexError):
        puzzle_4x4.row(5)


def test_get_col(puzzle_4x4, arr_4x4):
    assert np.all(puzzle_4x4.col(1) == arr_4x4[:, 1])


def test_neg_col(puzzle_4x4, arr_4x4):
    assert np.all(puzzle_4x4.col(-1) == arr_4x4[:, -1])


def test_invalid_col(puzzle_4x4):
    with pytest.raises(IndexError):
        puzzle_4x4.col(5)


def test_get_subgrid(puzzle_4x4, arr_4x4):
    assert np.all(puzzle_4x4.sub_grid(0, 0) == arr_4x4[0:2, 0:2])
    assert np.all(puzzle_4x4.sub_grid(0, 1) == arr_4x4[0:2, 2:4])


def test_subgrid_neg(puzzle_4x4, arr_4x4):
    assert np.all(puzzle_4x4.sub_grid(-1, -2) == arr_4x4[2:4, 0:2])


def test_indexing_single_number(puzzle_4x4):
    assert puzzle_4x4[0, 0] == 1
    assert puzzle_4x4[3, 3] == 16


def test_indexing_multi_number(puzzle_4x4, arr_4x4):
    assert np.all(puzzle_4x4[0, :] == arr_4x4[0, :])
    assert np.all(puzzle_4x4[0:None:2, 0:None:2] == np.array([[1, 3], [9, 11]]))


def test_indexing_multi_neg(puzzle_4x4, arr_4x4):
    assert np.all(puzzle_4x4[-1, -2] == arr_4x4[-1, -2])
    assert np.all(puzzle_4x4[-4:-2, -3:-1] == arr_4x4[-4:-2, -3:-1])
