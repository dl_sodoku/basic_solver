# Copyright George Barnett 2020

"""Testing rules

tests for validating sudoku if they are completed or not
"""

# Basic Imports
import pytest

# External Imports
import numpy as np

# Internal Imports
from sodokuAI.rules import valid_puzzle, valid_column, valid_row, valid_subgrid
from sodokuAI.puzzle import Puzzle

# Global Variables
# n/a


def rolling_solved_array(board_size: int) -> np.ndarray:
    puzzle = []
    num_subgrids = int(board_size ** 0.5)
    for i in range(0, num_subgrids):
        for j in range(0, num_subgrids):
            rolled_row = np.roll(np.arange(1, board_size + 1), i + j * num_subgrids)
            puzzle.append(rolled_row)
    return np.array(puzzle)


@pytest.fixture
def solved_9x9_arr() -> np.ndarray:
    """ solved 9,9 rolling puzzle"""
    yield rolling_solved_array(9)


@pytest.fixture
def invalid_first_last_entry_9x9_arr(solved_9x9_arr) -> np.ndarray:
    """ puzzle with 2 invalid entries

    0,0 and 8,8 location changed to invalid value so there is invalid rows, columns and first grids
    in first row, column and sub-grid there will be double 9s. In the last row, colum and subgrid there will be double 1s
    """
    solved_9x9_arr[0, 0] = 9
    solved_9x9_arr[:, :] = 1
    yield solved_9x9_arr


# TEST rows
def test_rows():
    assert valid_row(np.array([1, 2, 3]))
    assert valid_row(np.array([4, 2, 3, 1]))


def test_rows_invalid():
    assert not valid_row(np.array([4, 2, 2, 1]))
    assert not valid_row(np.array([5, 2, 3, 1]))


def test_rows_shape():
    with pytest.raises(ValueError):
        valid_row(np.array([[1, 2, 3]]))
    with pytest.raises(ValueError):
        valid_row(np.array([[1, 2], [3, 4]]))


# TEST columns
def test_columns():
    assert valid_column(np.array([1, 2, 3]))
    assert valid_column(np.array([4, 2, 3, 1]))


def test_columns_invalid():
    assert not valid_column(np.array([4, 2, 2, 1]))
    assert not valid_column(np.array([5, 2, 3, 1]))


def test_columns_shape():
    with pytest.raises(ValueError):
        valid_column(np.array([[1, 2, 3]]))
    with pytest.raises(ValueError):
        valid_column(np.array([[1, 2], [3, 4]]))


# TEST subgrids
def test_subgrid():
    assert valid_subgrid(np.array([[1, 2], [3, 4]]))
    assert valid_subgrid(np.array([[3, 1], [2, 4]]))


def test_subgrid_invalid():
    assert not valid_subgrid(np.array([[3, 5], [2, 4]]))
    assert not valid_subgrid(np.array([[3, 3], [2, 4]]))


def test_subgrid_shape():
    with pytest.raises(ValueError):
        valid_subgrid(np.array([1, 2, 3]))
    with pytest.raises(ValueError):
        valid_subgrid(np.array([[1, 2, 3], [4, 5, 6]]))


# TEST whole board
def test_puzzle(solved_9x9_arr):
    puzzle = Puzzle.from_numpy(solved_9x9_arr)
    assert valid_puzzle(puzzle)


def test_puzzle_invalid(invalid_first_last_entry_9x9_arr):
    puzzle = Puzzle.from_numpy(invalid_first_last_entry_9x9_arr)
    assert not valid_puzzle(puzzle)
