# Copyright George Barnett 2020

"""data loaindg

Basic class for training and evaluating model
"""

# Basic Imports
from typing import Tuple
import math

# External Imports
import numpy as np
import logging

# Internal Imports
# n/a

# Global Variables
logger = logging.getLogger(__name__)


class Puzzle:
    def __init__(self, board_size: int):
        if board_size < 1:
            raise ValueError("Board size must be positive integer")
        if board_size > 255:
            raise ValueError("Max board size supported is 255")
        if math.isqrt(board_size) != board_size ** 0.5:
            raise ValueError("Board is not a square number for sub-grids")
        logger.info(f"Creating puzzle size ({board_size},{board_size})")
        self.array = np.zeros([board_size, board_size], dtype=np.uint8)
        self.shape = self.array.shape
        self.sub_grid_shape = (math.isqrt(board_size), math.isqrt(board_size))

    @classmethod
    def from_numpy(cls, np_arr):
        if len(np_arr.shape) != 2:
            raise ArithmeticError("Only 2d numpy arrays can create puzzles")
        if np_arr.shape[0] != np_arr.shape[1]:
            raise ArithmeticError(
                "Numpy array must be square, only square puzzles are supported"
            )
        if np_arr.dtype not in [np.int8, np.int16, np.int32, np.int64]:
            raise TypeError("Invalid numpy array type")
        logger.info(f"Creating puzzle from numpy array: {np_arr}")
        new_puzzle = Puzzle(np_arr.shape[0])
        new_puzzle.array = np_arr
        new_puzzle.shape = np_arr.shape
        new_puzzle.sub_grid_shape = (
            math.isqrt(np_arr.shape[0]),
            math.isqrt(np_arr.shape[0]),
        )
        return new_puzzle

    def row(self, idx: int) -> np.ndarray:
        return self.array[idx]

    def col(self, idx: int) -> np.ndarray:
        return self.array[:, idx]

    def sub_grid(self, row: int, col: int) -> np.ndarray:
        sub_grid_mult = self.sub_grid_shape[0]
        row_start = sub_grid_mult * row
        row_end = sub_grid_mult * (row + 1)
        col_start = sub_grid_mult * col
        col_end = sub_grid_mult * (col + 1)
        # Replace 0s with None for negative indexing (-2:0 is invalid, -2:None is correct)
        row_start, row_end, col_start, col_end = [
            x if x != 0 else None for x in (row_start, row_end, col_start, col_end)
        ]
        return self.array[row_start:row_end, col_start:col_end]

    def __getitem__(self, item):
        return self.array[item]
