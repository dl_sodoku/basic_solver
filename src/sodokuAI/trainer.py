# Copyright George Barnett 2020

""" Trainer class

Basic class for training and evaluating model
"""

# Basic Imports
from pathlib import Path
from typing import List, Union, Any, Dict
import collections
import logging

# External Imports
import torch
from torch.utils.data import DataLoader
from torch.utils import tensorboard
from torch import optim, nn, Tensor
from pydantic import BaseModel, Field

# Internal Imports
from sodokuAI.data.dataset import (
    SodokuPairsDataset,
    SodokuTrainingDatasetsConfig,
)
from sodokuAI import lr_scheduler, model, utils, optimiser


# Global Variables
logger = logging.getLogger(__name__)


class RunnerConfig(BaseModel):
    exp_name: str
    datasets: SodokuTrainingDatasetsConfig
    max_epochs: int
    cuda: bool = False
    batch_size: int = 1000
    model: Union[model.ModelConfig]
    optimiser: optimiser.ConfigsUnion
    lr_scheduler: lr_scheduler.ConfigsOptions


class Runner:
    def __init__(
        self,
        log_dir: Union[Path, str],
        config: RunnerConfig,
    ):

        self.log_path = Path(log_dir)
        self.config = config

        self.device = (
            torch.device("cuda")
            if config.cuda and torch.cuda.is_available()
            else torch.device("cpu")
        )

        # Set up datasets
        ds_cfg = config.datasets
        if type(ds_cfg) == SodokuTrainingDatasetsConfig:
            dataset_type = SodokuPairsDataset
        else:
            raise ValueError(f"Invalid dataset type")

        self.train_dataloader = DataLoader(
            dataset=dataset_type(
                data_path=ds_cfg.train_path,
                cache=ds_cfg.cache,
            ),
            batch_size=config.batch_size,
            shuffle=True,
            num_workers=ds_cfg.workers,
            pin_memory=True,
        )
        self.val_dataloader = DataLoader(
            dataset=dataset_type(
                data_path=ds_cfg.val_path,
                cache=ds_cfg.cache,
            ),
            batch_size=config.batch_size,
            shuffle=False,
            num_workers=ds_cfg.workers,
            pin_memory=True,
        )

        self.tb_writer = tensorboard.SummaryWriter(log_dir=Path(self.log_path) / "tb")

        self.model = model.build_model(self.config.model)
        self.model = self.model.to(self.device)

        self.optimizer = optimiser.build_optimiser(
            self.model.parameters(), self.config.optimiser
        )
        self.lr_schedular = lr_scheduler.build_scheduler(
            self.optimizer, self.config.lr_scheduler
        )

        self.loss_function = nn.L1Loss()
        self.metric_functions = {
            "Accuracy": utils.ElementwiseAccuracy(reduction=None),
            "PuzzleAccuracy": utils.ElementwiseAccuracy(reduction="mean"),
        }

    def train(self, start_epoch: int = 1):

        for epoch in range(start_epoch, self.config.max_epochs):
            train_losses = self.train_epoch(epoch)
            logger.info(f"Epoch {epoch} loss: {train_losses.mean()}")
            self.tb_writer.add_histogram(
                "Epoch/Train/Loss/Distribution", train_losses, epoch
            )
            self.tb_writer.add_scalar("Epoch/Train/Loss", train_losses.mean(), epoch)

            validate_results = self.validate(self.val_dataloader, reduce=True)
            logger.info(f"Epoch {epoch} validation results: {validate_results}")

            if self.config.lr_scheduler.step_loop == "epoch":
                self.lr_schedular.step()
                logger.debug(
                    f"Learning rate set at {self.lr_schedular.get_last_lr()[0]}"
                )

    def train_epoch(self, epoch: int):
        batch_losses = []
        for i, (prob, sol) in enumerate(self.train_dataloader, 0):
            iter = epoch * len(self.train_dataloader) + i
            prob, sol = prob.to(self.device), sol.to(self.device)

            self.optimizer.zero_grad()
            outputs = self.model(prob)
            loss = self.loss_function(outputs, sol)
            self.tb_writer.add_scalar("Batch/Loss", loss.mean(), iter)
            logger.debug(f"{i+1}/{len(self.train_dataloader)} loss: {loss.item()}")

            batch_losses.append(loss)
            loss.backward()
            self.optimizer.step()

            self.tb_writer.add_scalar(
                "Batch/LearningRate", self.lr_schedular.get_last_lr()[0], iter
            )
            if self.config.lr_scheduler.step_loop == "iteration":
                self.lr_schedular.step()
                logger.debug(f"Learning rate set at {self.lr_schedular.get_lr()}")

        return torch.Tensor(batch_losses)

    def validate(self, dataloader: DataLoader, reduce: bool = False) -> Tensor:
        """Evaluate performance on a given dataloader

        Args:
            dataloader:
            reduce: Whether to return a list of performance on each problem

        Returns:

        """
        self.model.eval()
        with torch.no_grad():
            results: Dict[str, Tensor] = collections.defaultdict(list)
            for i, (prob, sol) in enumerate(dataloader):
                prob, sol = prob.to(self.device), sol.to(self.device)
                output = self.model(prob)

                # Iterate through performance metrics
                for metric_name, metric_fuction in self.metric_functions.items():
                    # TODO Identify across elements in batch
                    metric_result = metric_fuction(output, sol)
                    results[metric_name].append(metric_result)

            # Check all metrics are correct length
            assert all([len(dataloader) == len(v) for v in results.values()])

            if reduce:
                results = {
                    k: torch.stack(v, dim=0).float().mean() for k, v in results.items()
                }
            return results
