# Copyright George Barnett 2020

"""Model configs

"""

# Basic Imports
from typing import Union, Literal
import logging

# External Imports
from pydantic import BaseModel

# Internal Imports

# Global Variables
logger = logging.getLogger(__name__)


class HeadConfig(BaseModel):
    name: str


class BodyConfig(BaseModel):
    name: str


class BasicLinearConfig(BodyConfig):
    name: Literal["BasicLinear"]
    num_layers: int
    skip: bool


class ModelConfig(BaseModel):
    """Base optimiser config

    name value isn't passed to the optimiser, but all other values are.
    """

    body: Union[BasicLinearConfig]
