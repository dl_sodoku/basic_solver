# Copyright George Barnett 2020

"""Model functions  

"""

# Basic Imports
from typing import Literal, Union, Tuple
import collections
import logging

# External Imports
import torch
from torch import nn
from pydantic import BaseModel

from sodokuAI.model import configs

# Internal Imports
# Note is imported by Runner, so be careful about cyclical imports


# Global Variables
logger = logging.getLogger(__name__)


class LinearBlock(nn.Module):
    def __init__(
        self, num_features_in, num_features_out, relu=True, batchnorm=True, skip=False
    ):
        super(LinearBlock, self).__init__()
        self.skip = skip
        self.body = nn.Sequential(
            nn.Linear(num_features_in, num_features_out, bias=True),
            nn.ReLU(inplace=True),
            nn.BatchNorm1d(num_features=num_features_out),
        )

    def forward(self, input):
        if self.skip:
            return input + self.body(input)
        else:
            return self.body(input)


class BasicLinear(nn.Module):
    def __init__(self, num_layers=5, skip=True):
        super(BasicLinear, self).__init__()
        body_list = []
        for idx in range(num_layers):
            body_list.append(LinearBlock(81, 81, skip=skip))
        self.body = nn.Sequential(*body_list)

    def forward(self, x):
        x = x.view(-1, 81)
        x = self.body(x)
        return x.view(-1, 9, 9)


def _build_basic_linear(config: configs.BasicLinearConfig):
    return BasicLinear(config.num_layers, config.skip)
