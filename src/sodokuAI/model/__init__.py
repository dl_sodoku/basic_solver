# Copyright George Barnett 2020

"""Model functions  

"""

# Basic Imports
from typing import Literal, Union, Tuple
import collections
import logging

# External Imports
import torch
from torch import nn
from pydantic import BaseModel

# Internal Imports
from . import configs, basic_linear
from .configs import ModelConfig

# Global Variables
logger = logging.getLogger(__name__)


# Lookup for config.name and use to find model building function
_BODY_LOOKUP = {"BasicLinear": basic_linear._build_basic_linear}


def build_model(config: configs.ModelConfig) -> nn.Module:
    config_dict = dict(config)
    body = _BODY_LOOKUP[config.body.name](config.body)

    # Initialise optimiser function and return value
    return body


__all__ = [ModelConfig]
