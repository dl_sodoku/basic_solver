# Copyright George Barnett 2020

"""Configs

Load configs from a yaml file.

Hierachy is
    - passed arguments (highest priority)
    - exp_yaml
    - default vaules (lowest priority)

Values in exp_yaml and arguments must be in default values

Currently to support argparse typing, lists must contain elements of all the same type

"""

# Basic Imports
from typing import Any, Dict, List, Tuple, Union
from types import SimpleNamespace
import logging
from pathlib import Path
import argparse

# External Imports
import yaml
import copy

# Internal Imports
# n/a

# Global Variables
logger = logging.getLogger(__name__)


def read_yaml(yaml_path: Path) -> Dict[str, Any]:
    """ Read yaml file"""
    logger.debug(f"Reading yaml file: {yaml_path}")
    with open(str(yaml_path), "r") as yaml_file:
        output = yaml.safe_load(yaml_file)
    return output
