"""Utils Functions 
"""

# Basic Imports
import logging
import abc
from typing import Optional

# External Imports
import torch
from torch import nn, Tensor
import numpy as np
import logging

# Internal Imports
# n/a

# Global Variables
logger = logging.getLogger(__name__)


class LossFunction(nn.Module, abc.ABC):
    def __init__(
        self, reduction: Optional[str] = "mean", round: bool = False,
    ):
        """Abstract class for loss functions

        Generic base class for implementing useful functionality for losses.
        Essentially a re-implementation of the private pytorch nn._Loss class.

        """
        super().__init__()
        self._reduce_modes = [None, "mean", "sum", "none"]
        self._invalid_reduce = ValueError(
            f"Invalid reduce value: {reduction} is not a valid mode. Pick from {self._reduce_modes}"
        )
        if reduction not in self._reduce_modes:
            raise self._invalid_reduce
        self.reduction = reduction
        self.round = round

    def _round(self, prediction: Tensor, target: Tensor) -> Tensor:
        return prediction.round(), target.round()

    def _reduce_output(self, output: Tensor) -> Tensor:
        """Reduce output similar to generic loss functions
        """
        if self.reduction in [None, "none"]:
            return output
        elif self.reduction == "mean":
            return output.float().mean()
        elif self.reduction == "sum":
            return output.sum()
        else:
            raise self._invalid_reduce


class ElementwiseAccuracy(LossFunction):
    def __init__(
        self, reduction: Optional[str] = "mean", round: bool = False,
    ):
        """Accuracy per element"""
        super().__init__(reduction, round)

    def forward(self, prediction: Tensor, target: Tensor) -> Tensor:
        assert prediction.shape == target.shape
        if self.round:
            prediction, target = self._round(prediction, target)
        output = torch.eq(prediction, target)
        return self._reduce_output(output)

