# Copyright George Barnett 2020

"""Optimiser functions  

This is the management of optimser configs and loading the optimiser function

Steps to add new optimiser:
1. Add a OptimiserConfig (Can be a child of a previous one), with a name:Literal["<name>"]
as a required field. This is how the Config is selected, but it will not be
given as  kwarg to the optimiser function. If you want to do more complex logic
for optimiser arguments, implement a child class of the desired optimiser function  
2. Add entry to OPTIMISER_LOOKUP with an entry of "<name>":<function>
3. Add to the ConfigTypes Union. This means it is loadable by the RunnerConfig
"""

# Basic Imports
from typing import Literal, Union, Tuple
import collections
import logging

# External Imports
import torch
from torch import optim
from pydantic import BaseModel

# Internal Imports
# Note is imported by Runner, so be careful about cyclical imports

# Global Variables
logger = logging.getLogger(__name__)


class OptimiserConfig(BaseModel):
    """Base optimiser config
    
    name value isn't passed to the optimiser, but all other values are.
    """

    name: str
    lr: float = 0.1


class AdamConfig(OptimiserConfig):
    name: Literal["Adam"]
    eps: float = 1e-08
    betas: Tuple[float, float] = (0.9, 0.999)
    weight_decay: float = 0.99
    amsgrad: bool = False


class SGDConfig(OptimiserConfig):
    name: Literal["SGD"]
    momentum: float = 0
    dampening: float = 0
    weight_decay: float = 0
    nesterov: bool = False


# Lookup for config.name to optimiesr function
_OPTIMERSER_LOOKUP = {"Adam": optim.Adam, "SGD": optim.SGD}
ConfigsUnion = Union[AdamConfig, SGDConfig]


def build_optimiser(parameters, config: OptimiserConfig) -> optim.Optimizer:
    config_dict = dict(config)
    # Remove the name field as this isn't an argument for optimiser functions
    del config_dict["name"]
    # Lookup the optimiesr from the config.name field
    func = _OPTIMERSER_LOOKUP[config.name]
    logger.info(f"Using {config.name} optimiser with {config_dict}")
    # Initialise optimiser function and return value
    return func(parameters, **config_dict)

