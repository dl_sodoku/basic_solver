# Copyright George Barnett 2020

"""Sudoku Rules

Checking if a board is valid (completed)
"""

# Basic Imports
# n/a

# External Imports
import numpy as np
import logging

# Internal Imports
from sodokuAI.puzzle import Puzzle

# Global Variables
logger = logging.getLogger(__name__)


def valid_puzzle(puzzle: Puzzle):
    # TODO: parallelise
    # Using puzzles ensures proper shaping and valid elements
    num_rows, num_cols = puzzle.shape
    valid_rows = [valid_row(puzzle.row(i)) for i in range(num_rows)]
    valid_columns = [valid_column(puzzle.col(i)) for i in range(num_cols)]
    valid_subgrids = [
        valid_subgrid(puzzle.sub_grid(i, j))
        for i in range(puzzle.sub_grid_shape[0])
        for j in range(puzzle.sub_grid_shape[1])
    ]
    return all(valid_rows) and all(valid_columns) and all(valid_subgrids)


def valid_row(row: np.ndarray):
    if len(row.shape) != 1:
        raise ValueError("Invalid row shape, arrays must be 1d")
    arr = np.arange(1, len(row) + 1)
    return np.all(np.sort(row) == arr)


def valid_column(col: np.ndarray):
    if len(col.shape) != 1:
        raise ValueError("Invalid column shape, arrays must be 1d")
    arr = np.arange(1, len(col) + 1)
    return np.all(np.sort(col) == arr)


def valid_subgrid(sub_grid: np.ndarray):
    if len(sub_grid.shape) != 2:
        raise ValueError("Invalid subgrid shape, only 2d arrays are valid.")
    if sub_grid.shape[0] != sub_grid.shape[1]:
        raise ValueError("Invalid subgrid shape, array must be square")
    arr = np.arange(1, sub_grid.size + 1)
    flattened = sub_grid.flatten()
    return np.all(np.sort(flattened) == arr)
