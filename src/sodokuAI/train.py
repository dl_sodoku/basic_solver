# Copyright George Barnett 2020

""" Basic Training script
"""

# Basic Imports
from pathlib import Path
from typing import List, Tuple, Any, Dict
import logging
import argparse

# External Imports
from torch.utils.data import DataLoader
from torch.utils import tensorboard
from torch import optim, nn

# Internal Imports
from sodokuAI import configs
from sodokuAI.trainer import Runner, RunnerConfig

# Global Variables
logger = logging.basicConfig(level=logging.DEBUG)


def main(
    exp_config_path: Path, log_dir: Path = None,
):
    # Perform actual training and logic
    if log_dir is None:
        log_dir = exp_config_path.parent

    config_dict = configs.read_yaml(args.exp_yaml)
    config = RunnerConfig(**config_dict)
    trainer = Runner(log_dir, config,)
    trainer.train()


if __name__ == "__main__":
    # Parse args when script might be called from CLI
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-e", "--exp-yaml", type=Path, required=True,
    )
    parser.add_argument(
        "-l",
        "--log-dir",
        type=Path,
        default=None,
        help="The directory path to save all outputs, logs, checkpoint etc. Must be a directory",
    )
    args = parser.parse_args()

    main(args.exp_yaml, args.log_dir)
