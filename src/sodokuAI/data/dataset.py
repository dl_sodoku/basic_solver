# Copyright George Barnett 2020

"""data loaing

Basic class for training and evaluating model
"""

# Basic Imports
from pathlib import Path

from typing import List, Tuple
import csv
from pydantic import BaseModel

# External Imports
import torch
from torch.utils.data.dataset import Dataset
from torchvision import transforms
import numpy as np
import h5py

# Internal Imports
# n/a

# dataset is assumed from here:
# https://www.kaggle.com/bryanpark/sudoku


class SodokuTrainingDatasetsConfig(BaseModel):
    """Configuration for datasets during training
    
    Note this is for both datasets, not one per dataset """

    train_path: Path
    val_path: Path
    workers: int = 0
    cache: bool = False


class SodokuPairsDataset(Dataset):
    def __init__(self, data_path: Path, cache=False):
        """Dataset for laoding sodoku games

        Load a csv of 9x9 problems and solutions. the problems take the form of ints in the csv,
        and so must be converted to numpy int arrays.

        Args:
            data_path: Path to h5py file
        """
        data_file = h5py.File(data_path, "r")
        self.problems = data_file["problems"]
        self.solutions = data_file["solutions"]
        self.cache = cache
        if cache:
            self.problems = np.array(self.problems)
            self.solutions = np.array(self.solutions)

        self.classes = np.unique(self.solutions[0])

        self.transforms = transforms.Compose(
            [
                transforms.ToTensor(),
                transforms.Lambda(lambda x: x.squeeze(0)),
                transforms.Lambda(lambda x: x.float()),
            ]
        )

    def __len__(self) -> int:
        # return len(self.game_pairs)
        assert len(self.problems) == len(self.solutions)
        return len(self.problems)

    def __getitem__(self, item_idx: int) -> Tuple[np.ndarray, np.ndarray]:
        problem = torch.Tensor(self.problems[item_idx])
        solution = torch.Tensor(self.solutions[item_idx])

        return problem, solution
