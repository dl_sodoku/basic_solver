# Copyright George Barnett 2020

"""Loss Functions
"""

# Basic Imports
from typing import Literal, Union, Tuple
import collections
import logging

# External Imports
from torch.optim import lr_scheduler
from pydantic import BaseModel

# Internal Imports
# n/a

# Global Variables
logger = logging.getLogger(__name__)


class LRSchedulerConfig(BaseModel):
    """Base scheduler config
    Args:
        name: string to lookup in _SCHEDULER_LOOKUP
        step_lopo: When to step the scheduler, either after each epoch or after each iteration (forward pass)
    """

    name: str
    step_loop: Literal["epoch", "iteration"]


class StepLR(LRSchedulerConfig):
    name: Literal["Step"]
    step_size: float = 10
    gamma: float = 0.1


class CosineAnnealingConfig(LRSchedulerConfig):
    name: Literal["CosineAnnealing"]
    T_max: int = 100
    eta_min: float = 0


# How to lookup the OptimiserConfig.name to the function
_LRSCHEDULER_CONFIG = {
    "Step": lr_scheduler.StepLR,
    "CosineAnnealing": lr_scheduler.CosineAnnealingLR,
}
ConfigsOptions = Union[CosineAnnealingConfig, StepLR]


def build_scheduler(optimizer, config: LRSchedulerConfig) -> lr_scheduler._LRScheduler:
    config_dict = dict(config)
    del config_dict["name"]
    del config_dict["step_loop"]
    func = _LRSCHEDULER_CONFIG[config.name](optimizer, **config_dict)
    logger.info(f"Using {config.name} learnign rate scheduler with {config_dict}")
    return func
