# Basic Solver
## Installation

clone repository and create conda environment with: 

```
git clone 
conda env create --file requirements.yaml
pip install -e .
```


## Examples Puzzles
A commonly used example puzzle in this project is an example solved 9x9. This puzzle is referred to as a rolling puzzle due to the rows shifting on down each row. The puzzle is shown below. 

|  1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
|----|---|---|---|---|---|---|---|---|
| 7 | 8 | 9 | 1 | 2 | 3 | 4 | 5 | 6 |
| 4 | 5 | 6 | 7 | 8 | 9 | 1 | 2 | 3 |
| 9 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
| 6 | 7 | 8 | 9 | 1 | 2 | 3 | 4 | 5 |
| 3 | 4 | 5 | 6 | 7 | 8 | 9 | 1 | 2 |
| 8 | 9 | 1 | 2 | 3 | 4 | 5 | 6 | 7 |
| 5 | 6 | 7 | 8 | 9 | 1 | 2 | 3 | 4 |
| 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 1 |