from setuptools import setup, find_namespace_packages

setup(
    name="sodokuAI",
    package_dir={"": "src"},
    packages=find_namespace_packages(where="src"),
    author="George Barnett",
    description="Basic solver script for sodoku puzzles",
)
